import java.net.InetAddress;
import java.net.UnknownHostException;


/**
 *
 * @author jgoodwin
 */
public class ExerciceUn 
{
    public static void main(String[] args)
    {
        InetAddress local;
        try {
            //Adresse IP machine en cours
            System.out.println("Les références de la machine sont : "+InetAddress.getLocalHost());
            
            //Adresse IPs
            System.out.println("Les références de la machine sont : "+InetAddress.getByName("localhost"));
            System.out.println("Les références de la machine sont : "+InetAddress.getByName("www.uvsq.fr"));
            System.out.println("Les références de la machine sont : "+InetAddress.getByName("www.iut-velizy.uvsq.fr"));
            System.out.println("Les références de la machine sont : "+InetAddress.getByName("www.google.fr"));
            System.out.println("Les références de la machine sont : "+InetAddress.getByName("192.168.0.134"));
            
            //Adresse uvsq.fr
            byte[] adress = new byte[]{new Integer(193).byteValue(), new Integer(51).byteValue(), new Integer(27).byteValue(), new Integer(3).byteValue()};
            System.out.println("Les références de la machine sont : "+InetAddress.getByAddress(adress));
            
            //Récupère les adresses de elgoog
            InetAddress google[] = InetAddress.getAllByName("google.fr");
            for (InetAddress valeur : google)
            {
                System.out.println("adresse ip de google :"+valeur.getHostAddress());
            }
            
            
        } catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
    }
}
