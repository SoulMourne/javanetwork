import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;


/**
 *
 * @author jgoodwin
 */
public class ExerciceTrois 
{
    public static void main(String[] args) 
    {
        try {
            URL url = new URL("http://www.iut-velizy.uvsq.fr/");
            InputStreamReader in = new InputStreamReader(url.openStream());
            BufferedReader buff = new BufferedReader(in);
            String msg;
            while((msg = buff.readLine())!=null)
            {
                System.out.println(msg);
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
