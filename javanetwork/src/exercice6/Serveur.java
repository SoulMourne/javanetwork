/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package exercice6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author jgoodwin
 */
public class Serveur 
{
    
    private ServerSocket socketServer;//Variable du socket du serveur permettant aux clients de s'y connecter
    private BufferedReader in;  //Permet de lire des caractères
    private PrintWriter out;    //Permet d'écrire un message
    private Socket socketDuClient;  //Socket servant à communiquer avec le client
    
    public Serveur(int numPort)
    {
        socketDuClient = null;  //Initialisation d'un socket pour la communication avec le/les clients
        try {
            socketServer = new ServerSocket(numPort);  //Initialisation d'un ServerSocket sur le port 2009
            System.out.println("Le serveur est à l'écoute du port "+socketServer.getLocalPort());   //Indique sur quel port le serveur est à l'écoute
        } catch (IOException e) {   //En cas d'erreur
            e.printStackTrace();
        }
    }
    
    /**
     * Envoie une chaine de caractères au client via le socket correspondant
     * @param socketClient socket permettant de communiquer avec le client
     * @param message chaine de caractères à envoyer sur le socket
     * @return booléen si le message a été ou non envoyé
     */
    public boolean envoiMessage(Socket socketClient,String message)
    {
        try{
            out = new PrintWriter(socketClient.getOutputStream());   //Récupère l'OutputStream du socket du client et ouvre un PrintWriter permettant au serveur d'y écrire
            out.println(message); //Envoi d'un message au client ainsi que son adresse IP
            out.flush();    //Vide l'OutputStream
        }catch (IOException e){ //En cas d'erreur
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    /**
     * Lit un message envoyé via le socket du client
     * @param socketClient Le socket sur lequel le message est envoyé
     * @return la chaine de caractère lue ou bien null si erreur
     */
    public String lectureMessage(Socket socketClient)
    {
        try{
        in = new BufferedReader (new InputStreamReader (socketDuClient.getInputStream())); //permet de lire les caractères provenant du socketduserveur
        return in.readLine();   //Renvoie le contenu de in
        }catch (IOException e){ //En cas d'erreur
            e.printStackTrace();
            return null;
        }
    }
    
    public void ouvertureClient() throws IOException
    {
        socketDuClient = socketServer.accept();
        //System.out.println("Le client est connecté !\nSon adresse IP est "+socketDuClient.getRemoteSocketAddress());
    }
    
    public void fermetureClients()throws IOException
    {
            socketDuClient.close();
    }
    
    public Socket getSocketClient()
    {
        return this.socketDuClient;
    }
}
