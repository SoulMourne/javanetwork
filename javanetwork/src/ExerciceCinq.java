import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.Socket;
import java.net.URL;
import java.util.Scanner;


/**
 *
 * @author jgoodwin
 */
public class ExerciceCinq 
{
    public static void main(String[] args) 
    {
        try {
            String host = "192.168.0.215";
            String ressource = "/tpjava/";
            final int HTTP_PORT=80;
            
            System.out.println("A partir de " + host + "\n\n");
            
            //Création des sockets et des flux
            Socket s = new Socket(host, HTTP_PORT);
            InputStream entree = s.getInputStream();
            OutputStream sortie = s.getOutputStream();
            
            Scanner in = new Scanner(entree);
            PrintWriter out = new PrintWriter(sortie);
            
            String command= "GET "+ressource+" HTTP/1.1\n"+"Host: "+host+"\n\n";
            System.out.println(command);
            
            out.printf(command);
            out.flush();
            
            while(in.hasNextLine())
            {
                String input = in.nextLine();
                System.out.println(input);
            }
            s.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
