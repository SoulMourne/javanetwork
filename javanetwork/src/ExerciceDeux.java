import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;


/**
 *
 * @author jgoodwin
 */
public class ExerciceDeux 
{
    public static void main(String[] args) 
    {
        try {
            URL url = new URL("http://www.iut-velizy.uvsq.fr/index.php");
            String msg = "Nom d'Hote:"+ url.getHost();
            msg += "\nProtocole:"+url.getProtocol();
            msg += "\nPort précisé:"+url.getPort();
            msg += "\nPort par défaut:"+url.getDefaultPort();
            msg += "\nChemin et fichier:"+url.getFile();
            System.out.println(msg+"\n\n");
            
            url = new URL("http://dinfo215/tp/index.html");
            URLConnection conn = url.openConnection();
            msg = "drole de date : "+ conn.getDate();
            System.out.println(msg);
            int i = 0;
            while (conn.getHeaderField(i)!=null)
            {
                System.out.println("header "+i+" : "+conn.getHeaderField(i));
                i++;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
