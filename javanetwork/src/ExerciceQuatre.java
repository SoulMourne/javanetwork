import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 *
 * @author jgoodwin
 */
public class ExerciceQuatre 
{
    public static void main(String[] args) 
    {
        try {
            URL url = new URL("http://www.iut-velizy.uvsq.fr/");
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            String msg = "Méthode htpp pour la requête : " + conn.getRequestMethod();
            msg += "\ncode de réponse de l'Url : " + conn.getResponseCode();
            switch(conn.getResponseCode()){
                case 200:
                    msg += "\nRequête réussie";
                    break;
                case 404:
                    msg += "\nEchec de la requête Http";
                    break;
                default:
                    break;
            }
            msg += "\nla ligne numéro "+5+" de l'en-tête est : " + conn.getHeaderField(5-1);
            System.out.println(msg);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
