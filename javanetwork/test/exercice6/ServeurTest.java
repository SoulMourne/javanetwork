/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package exercice6;

import java.io.IOException;
import java.net.Socket;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jgoodwin
 */
public class ServeurTest {
    
    @Test
    public void testServeur() throws IOException 
    {
        Serveur serveur = new Serveur(5000);
        serveur.ouvertureClient();
        serveur.envoiMessage(serveur.getSocketClient(), "Bienvenue sur le serveur");
        String msg = serveur.lectureMessage(serveur.getSocketClient());
        System.out.println(msg);
        serveur.getSocketClient().close();
    }
}
