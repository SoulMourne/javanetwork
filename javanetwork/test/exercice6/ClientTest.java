/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package exercice6;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author jgoodwin
 */
public class ClientTest 
{
    @Test
    public void testClient() 
    {
        Client client = new Client("192.168.0.127", 5001);
        
        String msg = client.lectureMessage();
        System.out.println(msg);
        
        client.envoiMessage(client.getSocket().getLocalAddress().toString());
        
        client.fermetureClient();
    }
}
